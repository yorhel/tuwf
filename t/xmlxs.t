#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use Test::More;

BEGIN { use_ok 'TUWF::XMLXS', ':html5_', 'xml_string' };


sub lit {
    lit_ '<ok🥳ay>';
}

sub t {
    is $_[0], 'arg';
    div_ attr1 => $_[0], sub {
        is $_[0], 'arg';

        span_ 'ab" < c &< d';
        span_ \&lit;

        is xml_string(\&lit), '<ok🥳ay>';

        eval {
            xml_string { tag_ '<oops>', '' };
        };
        like $@, qr/Invalid tag or attribute name/;

        txt_ '🥳';
    };
}

is xml_string(sub { t 'arg' }), '<div attr1="arg"><span>ab&quot; &lt; c &amp;&lt; d</span><span><ok🥳ay></span>🥳</div>';

is xml_string(sub {
    tag_ 'customTag', 1;
    tag_ 'custom-selfclose', undef;
}), '<customTag>1</customTag><custom-selfclose />';

is xml_string(sub { txt_ '<hi>'; }), '&lt;hi>';

is xml_string(sub { div_ 'a-ttr', '"quoted', 'b', undef, '&'; }), '<div a-ttr="&quot;quoted">&amp;</div>';

eval { xml_string { span_ 'invalid attr' => 1, 1 } };
like $@, qr/Invalid tag or attribute name/;

eval { xml_string { span_ '' => 1, 1 } };
like $@, qr/Invalid tag or attribute name/;

is xml_string(sub { txt_ '🥳' }), '🥳';

is xml_string(sub {}), '';

is xml_string(sub {
    div_ x => 1, '+' => 2, '+', 3, undef;
}), '<div x="1 2 3" />';

is xml_string(sub {
    div_ x => 1, '+' => 2, '+', undef, undef;
}), '<div x="1 2" />';

is xml_string(sub {
    div_ x => 1, '+' => undef, '+', 3, undef;
}), '<div x="1 3" />';

is xml_string(sub {
    div_ x => 1, '+' => undef, y => undef, '+', 3, undef;
}), '<div x="1" y="3" />';

is xml_string(sub {
    div_ x => undef, '+' => undef, y => undef, '+', 3, undef;
}), '<div y="3" />';

is xml_string(sub {
    div_ x => undef, '+' => undef, '+', 1, undef;
}), '<div x="1" />';

eval { xml_string(sub { div_ '+' => 1, undef; }) };
like $@, qr/Cannot use '\+' as first attribute/;

eval { xml_string { span_ a => 1 } };
like $@, qr/Invalid number of arguments/;

eval { xml_string { span_ } };
like $@, qr/Invalid number of arguments/;

is xml_string(sub { br_ }), '<br />';

eval { xml_string { tag_ 'a' } };
like $@, qr/Invalid number of arguments/;

eval { xml_string { div_ sub { die "Hello"; } } };
like $@, qr/Hello/;

like xml_string(sub { div_ \1 }), qr/SCALAR\(/;
like xml_string(sub { div_ attr => \1, '' }), qr/SCALAR\(/;

done_testing;
