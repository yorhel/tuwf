package TUWF::XMLXS;

use strict;
use warnings;
use Exporter 'import';
use XSLoader;
use Carp 'confess';

our $VERSION = '1.5';

our @TAGS = qw/
    a abbr address area article aside audio b base bb bdo blockquote body br
    button canvas caption cite code col colgroup command datagrid datalist dd
    del details dfn dialog div dl dt em embed fieldset figure footer form h1 h2
    h3 h4 h5 h6 head header hr i iframe img input ins kbd label legend li link
    main map mark menu meta meter nav noscript object ol optgroup option output p
    param pre progress q rp rt ruby samp script section select small source
    span strong style sub summary sup table tbody td textarea tfoot th thead
    time title tr ul var video
/;

our %EXPORT_TAGS = (
    html5_ => [ qw/tag_ html_ lit_ txt_/, map "${_}_", @TAGS ],
);

our @EXPORT_OK = (
    qw/xml xml_string xml_escape html_escape/,
    @{$EXPORT_TAGS{html5_}},
);

XSLoader::load('TUWF::XMLXS');


sub xml_string(&) {
    my $oldbuf = swapbuffer(undef);
    eval { &{$_[0]}(); };
    my $buf = swapbuffer($oldbuf);
    die $@ if $@;
    $buf
}

my %XML = qw/& &amp; < &lt; " &quot;/;
sub xml_escape {
    local $_ = $_[0];
    if(!defined $_) {
        confess "Attempting to XML-escape an undefined value";
        return '';
    }
    s/([&<"])/$XML{$1}/g;
    $_;
}

sub html_escape {
    local $_ = xml_escape shift;
    s/\r?\n/<br \/>/g;
    $_;
}

sub html_ {
    lit_("<!DOCTYPE html>\n");
    tag_('html', @_);
}

sub xml {
    lit_('<?xml version="1.0" encoding="UTF-8"?>');
}

# Nicer API idea: rename xml_string() to fragment(), make xml() and html() return a string.
# Alas, my goal was for this module to be a compatible subset of TUWF::XML. Maybe later.
