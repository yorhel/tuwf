#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"


// The Perl string we're supposed to be writing to.
static SV *outbuf = NULL;

// Internal buffer we're actually writing to.
// Custom buffering is noticeably faster than calling the sv_cat* functions.
static size_t outbuf_len = 0, outbuf_size = 0;
static char *outbuf_buf = NULL;


static void bufgrow(size_t n) {
    if (outbuf_size == 0) outbuf_size = 4096;
    while (outbuf_size < n + outbuf_len)
        outbuf_size = outbuf_size << 2;
    outbuf_buf = saferealloc(outbuf_buf, outbuf_size);
}

#define bufwrite(str, n) do {\
        if (__builtin_expect(outbuf_size < (n) + outbuf_len, 0)) bufgrow(n);\
        memcpy(outbuf_buf+outbuf_len, str, (n));\
        outbuf_len += (n);\
    } while(0)


__attribute__((cold, format (printf, 1, 2)))
static void confess(char* pat, ...) {
    va_list args;
    SV *sv;
    dTHX;
    dSP;

    va_start(args, pat);
    sv = vnewSVpvf(pat, &args);
    va_end(args);

    ENTER;
    SAVETMPS;
    PUSHMARK(SP);
    XPUSHs(sv_2mortal(sv));
    PUTBACK;
    call_pv("Carp::confess", G_DISCARD);
    FREETMPS;
    LEAVE;
}


static void escape(const char *str) {
    size_t len;
    char x;
    while (1) {
        len = 0;
        while (1) {
            x = str[len];
            if (!x || x == '<' || x == '&' || x == '"') break;
            len++;
        }
        bufwrite(str, len);
        switch (x) {
            case 0: return;
            case '<': bufwrite("&lt;", 4); break;
            case '&': bufwrite("&amp;", 5); break;
            case '"': bufwrite("&quot;", 6); break;
        }
        str += len + 1;
    }
}


static int isvalidchar(unsigned int x) {
    return (x|32)-'a' < 26 || x-'0' < 10 || x == '_' || x == ':' || x == '-';
}

// Validate a tag or attribute name. Pretty much /^[a-z0-9_:-]+$/i.
// This does not at all match with the XML and HTML standards, but this
// approach is simpler and catches the most important bugs anyway.
static void validate_name(const char *str) {
    const char *x = str;
    while (isvalidchar(*x)) x++;
    if (*x || x == str) confess("Invalid tag or attribute name: '%s'", str);
}


// Caller is supposed to output "<tag" and, if this function returns true, "</tag>"
static int tag_(pTHX_ I32 ax, I32 offset, I32 argc, int selfclose) {
    STRLEN keyl, vall;
    SV *key, *val;
    char *keys, *vals, *lastkey = NULL;
    int isopen = 0;
    dSP;

    if (!selfclose && ((argc - offset) & 1) == 0) confess("Invalid number of arguments");

    while (offset < argc-1) {
        key = ST(offset);
        offset++;
        val = ST(offset);
        offset++;

        // Don't even try to stringify other arguments; non-string keys are always a bug.
        if (!SvPOK(key)) confess("Non-string attribute");
        keys = SvPVutf8(key, keyl);

        SvGETMAGIC(val);
        vals = SvOK(val) ? SvPVutf8(val, vall) : NULL;

        if (keyl == 1 && *keys == '+') {
            if (!vals) {
                // ignore
            } else if (isopen) {
                bufwrite(" ", 1);
                escape(vals);
            } else if (lastkey) {
                keyl = strlen(lastkey);
                bufwrite(" ", 1);
                bufwrite(lastkey, keyl);
                bufwrite("=\"", 2);
                escape(vals);
                isopen = 1;
            } else {
                confess("Cannot use '+' as first attribute");
            }
        } else {
            if (isopen) {
                bufwrite("\"", 1);
                isopen = 0;
            }
            validate_name(keys);
            if (!vals) {
                lastkey = keys;
            } else {
                bufwrite(" ", 1);
                bufwrite(keys, keyl);
                bufwrite("=\"", 2);
                escape(vals);
                isopen = 1;
            }
        }
    }

    if (isopen) bufwrite("\"", 1);

    if (offset < argc) {
        val = ST(offset);
        SvGETMAGIC(val);
    } else
        val = &PL_sv_undef;

    if (!SvOK(val)) { // undef
        bufwrite(" />", 3);
        return 0;
    } else if (SvROK(val) && strcmp(sv_reftype(SvRV(val), 0), "CODE") == 0) { // CODE ref
        bufwrite(">", 1);
        PUSHMARK(SP);
        call_sv(val, G_VOID|G_DISCARD|G_NOARGS);
        return 1;
    } else {
        bufwrite(">", 1);
        // XXX: This will encode non-CODE refs as ugly Perl strings (e.g.
        // "SCALAR(0x...)"), which we literally *never* want to happen. Not
        // super easy to check for that, though, because we do want to support
        // stringifying references that have been usefully overloaded.
        escape(SvPVutf8_nolen(val));
        return 1;
    }
}


#define PTAG(s, c) \
    bufwrite("<"s, sizeof s);\
    if (tag_(aTHX_ ax, 0, items, c))\
        bufwrite("</"s">", 2 + sizeof s);

/*
  require './lib/TUWF/XMLXS.pm';
  print sprintf qq{\nvoid %s_(...)\n  CODE:\n    PTAG("%1\$s", %d)\n},
    $_, /^(area|base|br|col|command|embed|hr|img|input|link|meta|param|source)$/?1:0
    for @TUWF::XMLXS::TAGS;
*/


MODULE = TUWF::XMLXS   PACKAGE = TUWF::XMLXS

SV *swapbuffer(buf)
    SV *buf
  CODE:
    if (outbuf_len) {
        if (outbuf) { // original buffer isn't undef, append to it
            sv_catpvn_flags(outbuf, outbuf_buf, outbuf_len, SV_CATUTF8);
            safefree(outbuf_buf);
        } else { // was undef, pass our buffer to to an SV
            bufwrite("", 1); // trailing nul
            outbuf = newSV(0);
            sv_usepvn_flags(outbuf, outbuf_buf, outbuf_len-1, SV_HAS_TRAILING_NUL);
            SvUTF8_on(outbuf);
        }
    } else if (!outbuf)
        outbuf = newSVpvs("");
    RETVAL = outbuf;
    outbuf = SvOK(buf) ? SvREFCNT_inc_simple(buf) : NULL;
    outbuf_len = outbuf_size = 0;
    outbuf_buf = NULL;
  OUTPUT:
    RETVAL


void lit_(arg)
    SV *arg
  PREINIT:
    STRLEN len;
    char *str;
  CODE:
    str = SvPVutf8(arg, len);
    bufwrite(str, len);


void txt_(str)
    char *str
  CODE:
    escape(str);


void tag_(arg, ...)
    SV *arg
  PREINIT:
    STRLEN len;
    char *str;
  CODE:
    str = SvPVutf8(arg, len);
    validate_name(str);
    bufwrite("<", 1);
    bufwrite(str, len);
    if (tag_(aTHX_ ax, 1, items, 0)) {
        bufwrite("</", 2);
        bufwrite(str, len);
        bufwrite(">", 1);
    }


void a_(...)
  CODE:
    PTAG("a", 0)

void abbr_(...)
  CODE:
    PTAG("abbr", 0)

void address_(...)
  CODE:
    PTAG("address", 0)

void area_(...)
  CODE:
    PTAG("area", 1)

void article_(...)
  CODE:
    PTAG("article", 0)

void aside_(...)
  CODE:
    PTAG("aside", 0)

void audio_(...)
  CODE:
    PTAG("audio", 0)

void b_(...)
  CODE:
    PTAG("b", 0)

void base_(...)
  CODE:
    PTAG("base", 1)

void bb_(...)
  CODE:
    PTAG("bb", 0)

void bdo_(...)
  CODE:
    PTAG("bdo", 0)

void blockquote_(...)
  CODE:
    PTAG("blockquote", 0)

void body_(...)
  CODE:
    PTAG("body", 0)

void br_(...)
  CODE:
    PTAG("br", 1)

void button_(...)
  CODE:
    PTAG("button", 0)

void canvas_(...)
  CODE:
    PTAG("canvas", 0)

void caption_(...)
  CODE:
    PTAG("caption", 0)

void cite_(...)
  CODE:
    PTAG("cite", 0)

void code_(...)
  CODE:
    PTAG("code", 0)

void col_(...)
  CODE:
    PTAG("col", 1)

void colgroup_(...)
  CODE:
    PTAG("colgroup", 0)

void command_(...)
  CODE:
    PTAG("command", 1)

void datagrid_(...)
  CODE:
    PTAG("datagrid", 0)

void datalist_(...)
  CODE:
    PTAG("datalist", 0)

void dd_(...)
  CODE:
    PTAG("dd", 0)

void del_(...)
  CODE:
    PTAG("del", 0)

void details_(...)
  CODE:
    PTAG("details", 0)

void dfn_(...)
  CODE:
    PTAG("dfn", 0)

void dialog_(...)
  CODE:
    PTAG("dialog", 0)

void div_(...)
  CODE:
    PTAG("div", 0)

void dl_(...)
  CODE:
    PTAG("dl", 0)

void dt_(...)
  CODE:
    PTAG("dt", 0)

void em_(...)
  CODE:
    PTAG("em", 0)

void embed_(...)
  CODE:
    PTAG("embed", 1)

void fieldset_(...)
  CODE:
    PTAG("fieldset", 0)

void figure_(...)
  CODE:
    PTAG("figure", 0)

void footer_(...)
  CODE:
    PTAG("footer", 0)

void form_(...)
  CODE:
    PTAG("form", 0)

void h1_(...)
  CODE:
    PTAG("h1", 0)

void h2_(...)
  CODE:
    PTAG("h2", 0)

void h3_(...)
  CODE:
    PTAG("h3", 0)

void h4_(...)
  CODE:
    PTAG("h4", 0)

void h5_(...)
  CODE:
    PTAG("h5", 0)

void h6_(...)
  CODE:
    PTAG("h6", 0)

void head_(...)
  CODE:
    PTAG("head", 0)

void header_(...)
  CODE:
    PTAG("header", 0)

void hr_(...)
  CODE:
    PTAG("hr", 1)

void i_(...)
  CODE:
    PTAG("i", 0)

void iframe_(...)
  CODE:
    PTAG("iframe", 0)

void img_(...)
  CODE:
    PTAG("img", 1)

void input_(...)
  CODE:
    PTAG("input", 1)

void ins_(...)
  CODE:
    PTAG("ins", 0)

void kbd_(...)
  CODE:
    PTAG("kbd", 0)

void label_(...)
  CODE:
    PTAG("label", 0)

void legend_(...)
  CODE:
    PTAG("legend", 0)

void li_(...)
  CODE:
    PTAG("li", 0)

void link_(...)
  CODE:
    PTAG("link", 1)

void main_(...)
  CODE:
    PTAG("main", 0)

void map_(...)
  CODE:
    PTAG("map", 0)

void mark_(...)
  CODE:
    PTAG("mark", 0)

void menu_(...)
  CODE:
    PTAG("menu", 0)

void meta_(...)
  CODE:
    PTAG("meta", 1)

void meter_(...)
  CODE:
    PTAG("meter", 0)

void nav_(...)
  CODE:
    PTAG("nav", 0)

void noscript_(...)
  CODE:
    PTAG("noscript", 0)

void object_(...)
  CODE:
    PTAG("object", 0)

void ol_(...)
  CODE:
    PTAG("ol", 0)

void optgroup_(...)
  CODE:
    PTAG("optgroup", 0)

void option_(...)
  CODE:
    PTAG("option", 0)

void output_(...)
  CODE:
    PTAG("output", 0)

void p_(...)
  CODE:
    PTAG("p", 0)

void param_(...)
  CODE:
    PTAG("param", 1)

void pre_(...)
  CODE:
    PTAG("pre", 0)

void progress_(...)
  CODE:
    PTAG("progress", 0)

void q_(...)
  CODE:
    PTAG("q", 0)

void rp_(...)
  CODE:
    PTAG("rp", 0)

void rt_(...)
  CODE:
    PTAG("rt", 0)

void ruby_(...)
  CODE:
    PTAG("ruby", 0)

void samp_(...)
  CODE:
    PTAG("samp", 0)

void script_(...)
  CODE:
    PTAG("script", 0)

void section_(...)
  CODE:
    PTAG("section", 0)

void select_(...)
  CODE:
    PTAG("select", 0)

void small_(...)
  CODE:
    PTAG("small", 0)

void source_(...)
  CODE:
    PTAG("source", 1)

void span_(...)
  CODE:
    PTAG("span", 0)

void strong_(...)
  CODE:
    PTAG("strong", 0)

void style_(...)
  CODE:
    PTAG("style", 0)

void sub_(...)
  CODE:
    PTAG("sub", 0)

void summary_(...)
  CODE:
    PTAG("summary", 0)

void sup_(...)
  CODE:
    PTAG("sup", 0)

void table_(...)
  CODE:
    PTAG("table", 0)

void tbody_(...)
  CODE:
    PTAG("tbody", 0)

void td_(...)
  CODE:
    PTAG("td", 0)

void textarea_(...)
  CODE:
    PTAG("textarea", 0)

void tfoot_(...)
  CODE:
    PTAG("tfoot", 0)

void th_(...)
  CODE:
    PTAG("th", 0)

void thead_(...)
  CODE:
    PTAG("thead", 0)

void time_(...)
  CODE:
    PTAG("time", 0)

void title_(...)
  CODE:
    PTAG("title", 0)

void tr_(...)
  CODE:
    PTAG("tr", 0)

void ul_(...)
  CODE:
    PTAG("ul", 0)

void var_(...)
  CODE:
    PTAG("var", 0)

void video_(...)
  CODE:
    PTAG("video", 0)
